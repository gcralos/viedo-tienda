import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PeliculaModel } from '../../model/peliculas.model';

@Component({
  selector: 'app-list-peliculas',
  templateUrl: './list-peliculas.component.html',
  styleUrls: ['./list-peliculas.component.scss'],
})
export class ListPeliculasComponent implements OnInit {
  @Input() peliculasInput: PeliculaModel;
  @Output() peliculaOutput: EventEmitter<any> = new EventEmitter();
  isVisibleInput: boolean;
  listOfColumn = [
    {title: 'id'},
    {title: 'Nombre', priority: 3 },
    {title: 'Fecha  Disponibilidad', priority: 2 },
    {title: 'estado ', priority: 1 },
  ];

  constructor() {
    this.isVisibleInput = false;
  }

  ngOnInit(): void {}

  editar(peliculas: any): void {
    this.peliculaOutput.emit(peliculas);
  }
  delete(peliculas: any){
    console.log('carlos');
  }
}
