import { Component, OnInit } from '@angular/core';
import { PeliculaModel } from '../model/peliculas.model';
import {FormGroup, FormControl, FormBuilder} from '@angular/forms';
import { PeliculaServices } from '../services/pelicula-services';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.scss'],
})
export class PeliculaComponent implements OnInit {
  peliculasModel: PeliculaModel[];
  formPelicula: FormGroup;
  isVisible: boolean;
  tituloModal: string;
  constructor(private peliculaServices: PeliculaServices,
              private formBuilderPeliculas: FormBuilder) {
    this.isVisible = false;
    this.tituloModal = '';
    this.formFilm();
  }

  ngOnInit(): void {
    this.getFilm();

  }
  getFilm(){
    this.peliculaServices.getFilm().subscribe((res) => {
       this.peliculasModel = res;
    });
  }
  onAddFilm() {
    this.showModal();
    this.tituloModal = 'Nueva pelicula';
    this.formPelicula.reset('');
  }
  onUpdateFilm(peliculas: any) {
    this.setFormFilm(peliculas);
    this.tituloModal = 'Editar Pelicula';
    this.showModal();

  }
  saveFilm() {
    this.peliculaServices.addFilm(this.formPelicula.value);
    this.formPelicula.reset('');
    this.handleCancel();
  }

  formFilm(){
    return    this.formPelicula =  this.formBuilderPeliculas.group({
      nombrePelicula: [''],
      fechaPublicacion: [''],
      estado: ['']
    });
  }
  setFormFilm(peliculas: any){
    const date = new Date(peliculas.fechaPublicacion);
    this.formPelicula.get('nombrePelicula').setValue(peliculas.nombrePelicula);
    this.formPelicula.get('estado').setValue(peliculas.estado);
  }

  showModal( ): void {
    this.isVisible = true;
  }


  handleCancel(): void {
    this.isVisible = false;
  }

}
