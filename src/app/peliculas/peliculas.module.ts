import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPeliculasComponent } from './list-peliculas/list-peliculas.component';
import { PeliculasRoutingModule } from './peliculas-routing.module';
import { NgAntdComponentUiModule } from '../ng-antd-component-ui/ng-antd-component-ui.module';
import { PeliculaComponent } from './pelicula.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DataformaPipe } from '../pipe/dataforma.pipe';
import { ListTurnosComponent } from './list-turnos/list-turnos.component';
@NgModule({
  declarations: [ListPeliculasComponent, PeliculaComponent, DataformaPipe, ListTurnosComponent],
  imports: [
    CommonModule,
    PeliculasRoutingModule,
    NgAntdComponentUiModule,
    ReactiveFormsModule,
  ],
  exports: [PeliculasRoutingModule],
})
export class PeliculasModule {}
