import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PeliculaComponent } from './pelicula.component';
import {ListTurnosComponent} from './list-turnos/list-turnos.component';
const routes: Routes = [
    { path: '', component: PeliculaComponent,
      children: [
        {path: 'list-turnos', component: ListTurnosComponent}
      ] }
  ];
@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PeliculasRoutingModule {}
