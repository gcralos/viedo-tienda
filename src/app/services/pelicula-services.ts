import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PeliculaModel } from '../model/peliculas.model';
@Injectable({
  providedIn: 'root',
})
export class PeliculaServices {
  private peliculasColletion: AngularFirestoreCollection<PeliculaModel>;
  peliculas: Observable<PeliculaModel[]>;
  constructor(private readonly angularFirestore: AngularFirestore) {
    this.peliculasColletion = angularFirestore.collection<PeliculaModel>(
      'peliculas'
    );

    this.peliculas = this.peliculasColletion.snapshotChanges().pipe(
      map((action) =>
        action.map((a) => {
          const data = a.payload.doc.data() as PeliculaModel;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      )
    );
  }

  getFilm() {
    return this.peliculas;
  }
  addFilm(pelicula: PeliculaModel) {
    return this.peliculasColletion.add(pelicula);
  }
   updateFilm(pelicula: PeliculaModel){

   }
}
