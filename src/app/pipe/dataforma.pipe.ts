import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dataforma',
})
export class DataformaPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    console.log(value.seconds);
    const resul = new Intl.DateTimeFormat('en-US').format(value.seconds);
    return resul;
  }
}
