export interface PeliculaModel {
  id: number;
  nombrePelicula: string;
  fechaPublicacion: Date;
  estado: boolean;
}
